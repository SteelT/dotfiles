#!/bin/bash

STEP=5
ICON="display-brightness-symbolic"

STEP=5

if [ "$1" == "inc" ] ; then
    brightnessctl -q s +$STEP%
fi

if [ "$1" == "dec" ]; then
   brightnessctl -q s $STEP%-
fi

if [ "$1" == "mute" ]; then
   pamixer -t
fi

BRIGHTNESS=$(brightnessctl -m | awk -F, '{print substr($4, 0, length($4)-1)}')

notify-send.sh -R "$XDG_RUNTIME_DIR/brightness-osd.id" -a "osd-popup" "Brightness" "$BRIGHTNESS%" --hint string:image-path:$ICON --hint int:has-percentage:$BRIGHTNESS --hint int:transient:1