#!/bin/bash

STEP=5

if [ "$1" == "inc" ] ; then
   pamixer -i $STEP
fi

if [ "$1" == "dec" ]; then
   pamixer -d $STEP
fi

if [ "$1" == "mute" ]; then
   pamixer -t
fi

VOLUME=$(pamixer --get-volume)
MUTE=$(pamixer --get-mute)

if [ "$VOLUME" -le 20 ]; then
    ICON=audio-volume-low
else if [ "$VOLUME" -le 60 ]; then
         ICON=audio-volume-medium
     else 
         ICON=audio-volume-high
     fi
fi
if [ "$MUTE" == "true" ]; then
    ICON=audio-volume-muted
fi

notify-send.sh -R "$XDG_RUNTIME_DIR/volume-osd.id" -a "osd-popup" "Volume" "$VOLUME%" --hint string:image-path:$ICON --hint int:has-percentage:$VOLUME --hint int:transient:1