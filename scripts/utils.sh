#!/bin/sh
BOLD="\e[1m"
GREEN="${BOLD}\e[32m"
DEF="\e[0m"

readonly DEF BOLD GREEN

print_msg() {
  printf "%b==>%b%b %s%b\n" "$GREEN" "$DEF" "$BOLD" "$@" "$DEF"
}
