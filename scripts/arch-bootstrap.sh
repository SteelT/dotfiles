#!/bin/sh
# shellcheck source=/dev/null
set -e

BOOTSTRAP_DIR="$(mktemp -d)"

clean_up () {
    rm -rf "${BOOTSTRAP_DIR}"
} 
trap clean_up EXIT

print_msg "Installing bootstrap packages..."
sudo pacman -S git base-devel

curl https://aur.archlinux.org/cgit/aur.git/snapshot/paru-git.tar.gz | tar --strip-components=1 -xz -C "$BOOTSTRAP_DIR"
cd "$BOOTSTRAP_DIR/" || exit 
makepkg -scif
