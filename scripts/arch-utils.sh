#!/bin/sh
_isPackageNotInstalled() {
  pacman -Qi "$1" >/dev/null 2>&1
  echo $?
}
