# ls
alias l='ls -lh'
alias ll='ls -lah'
alias la='ls -A'
alias lm='ls -m'
alias lr='ls -R'
alias lg='ls -l --group-directories-first'
alias lsd='ls -alt'

# misc
alias putclip='xclip -selection clipboard'
alias getclip='xclip -selection clipboard -o'
alias zshrc="micro $_ZSH_RC_FILE"
alias cz=chezmoi
alias clearclip='xclip -sel clip < /dev/null'
alias lspkgs="pacman -Qq | fzf --preview 'bkt --ttl=10m --stale=10s --scope=pacman_pkgs -- pacman -Qil {}' --layout=reverse --bind 'enter:execute(pacman -Qil {} | less)'"
alias showlog="journalctl -b --user"
alias wget=wget --hsts-file="$XDG_DATA_HOME/wget-hsts"
alias rgf='rg --files | rg'
alias updatemirrors='sudo reflector --latest 10 --protocol https --sort rate --country US --save /etc/pacman.d/mirrorlist'
alias instpkg="paru -Slq | fzf --multi --preview 'bkt --ttl=10m --stale=10s --scope=pacman_instpkgs -- paru -Si {1}' --layout=reverse | xargs -ro paru -S"
alias rmpkg="pacman -Qq | fzf --multi --preview 'bkt --ttl=10m --stale=10s --scope=pacman_rmpkgs -- pacman -Qi {1}' --layout=reverse | xargs -ro sudo pacman -Rns"
