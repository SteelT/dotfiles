# History file location
HISTFILE="$_ZSH_STATE_DIR/history"

# Turn off "no matches found" on glob failure
unsetopt NOMATCH

# Remove older command from the history if a duplicate is to be added.
setopt HIST_IGNORE_ALL_DUPS

# don't share history between terminals
unsetopt SHARE_HISTORY

# Append to the history file
setopt APPEND_HISTORY

# Set what highlighters will be used
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets)

# Make comments be visible
ZSH_HIGHLIGHT_STYLES[comment]=fg=242,bold

# Customize spelling correction prompt.
SPROMPT='zsh: correct %F{red}%R%f to %F{green}%r%f [nyae]? '

# Remove path separator from WORDCHARS.
WORDCHARS=${WORDCHARS//[\/]}

# Disable automatic widget re-binding on each precmd. This can be set when
# zsh-users/zsh-autosuggestions is the last module in your ~/.zimrc.
ZSH_AUTOSUGGEST_MANUAL_REBIND=1