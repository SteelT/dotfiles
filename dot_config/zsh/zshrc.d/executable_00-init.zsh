# Define zimfw location
ZIM_HOME="${XDG_DATA_HOME:-$HOME/.local/share}/zimfw"

# Create dirs if needed
if [ ! -d "$_ZSH_STATE_DIR" ]; then
  mkdir -p "$_ZSH_STATE_DIR"
fi

if [ ! -d "$_ZSH_CACHE_DIR" ]; then
  mkdir -p "$_ZSH_CACHE_DIR"
fi

fpath=($ZDOTDIR/completions $fpath)

zstyle ':completion:*' rehash true
zstyle ':zim:duration-info' threshold 0.5
zstyle ':autocomplete:*' min-input 1 # characters
zstyle ':zim:completion' dumpfile ${_ZSH_CACHE_DIR}/zcompdump
zstyle ':completion::complete:*' cache-path ${_ZSH_CACHE_DIR}/zcompcache

# Download zimfw plugin manager if missing.
if [[ ! -e ${ZIM_HOME}/zimfw.zsh ]]; then
	curl -fsSL --create-dirs -o ${ZIM_HOME}/zimfw.zsh \
			https://github.com/zimfw/zimfw/releases/latest/download/zimfw.zsh
fi

# Install missing modules, and update ${ZIM_HOME}/init.zsh if missing or outdated.
if [[ ! ${ZIM_HOME}/init.zsh -nt ${ZDOTDIR:-${HOME}}/.zimrc ]]; then
	source ${ZIM_HOME}/zimfw.zsh init -q
fi

# Initialize modules.
source ${ZIM_HOME}/init.zsh

autoload -Uz add-zsh-hook
