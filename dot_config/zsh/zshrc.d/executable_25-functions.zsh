ytdlm () {
	tmp_dir="$(mktemp -u)"
	yt-dlp \
		--mp3 \
		--square-thumb \
		--no-playlist \
		--parse-metadata "playlist_index:(?s)(?P<track_number>.+)" \
		--parse-metadata "release_year:(?s)(?P<meta_date>.+)" \
		--embed-metadata \
		-P 'home:~/Music/' \
		-P "temp:${tmp_dir}" \
		-o '%(artist)s - %(album)s/%(playlist_index)02d%(playlist_index& - |)s%(title)s.%(ext)s' \
		"$@"
	rm -vrf "${tmp_dir}"	
}
