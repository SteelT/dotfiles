{
    "spacing": 4,
    "height": 40,
    "modules-left": ["wlr/taskbar"],
    "modules-right": [
    	"tray",
    	"pulseaudio#output",
    	"pulseaudio#input",
    	"network",  
    	"battery",
    	"backlight",
		"custom/swaync",
		"clock"
    ],
    "tray": {
    	"icon-size": 24,
    	"spacing": 10,
    	"reverse-direction": true
    },
    "clock": {
        "tooltip": true,
        "format": " {:%I:%M %p}",
        "format-alt": "{:%Y-%m-%d}",
        "tooltip-format": "<tt><small>{calendar}</small></tt>",
        "calendar": {
                    "mode"          : "month",
                    "mode-mon-col"  : 3,
                    "weeks-pos"     : "",
                    "on-scroll"     : 1,
                    "format": {
                              "months":     "<span color='#565656'><b>{}</b></span>",
                              "days":       "<span color='#a9a9a9'><b>{}</b></span>",
                              "weeks":      "<span color='#878787'><b>W{}</b></span>",
                              "weekdays":   "<span color='#a8a9ad'>{}</span>",
                              "today":      "<span color='#000000'><b><u>{}</u></b></span>"
                              }
                    },
        "actions":  {
                    "on-click-right": "mode",
                    "on-scroll-up": "tz_up",
                    "on-scroll-down": "tz_down",                    
                    "on-scroll-up": "shift_up",
                    "on-scroll-down": "shift_down"
                    }
    },
    "backlight": {
        "format": "{icon} {percent}% ",
        "format-icons": ["", "", "", "", "", "", "", "", ""]
    },
    "battery": {
        "interval": 2,
        "states": {
            "warning": 15,
            "low": 10,
            "critical": 4
        },
        "format": "{icon} {capacity}%",
        "tooltip-format": "{timeTo} ({capacity}%)",
	    "format-icons": ["", "", "", "", ""],
	    "max-length": 25
    },
    "network": {
        "interval": 2,
        "format-icons": {
            "wifi": ["󰤯", "󰤟", "󰤢", "󰤥", "󰤨"],
            "ethernet": "󰈀",
            "disconnected": "󰌙",
            "disabled": "󰲜"
        },
        "format": "{icon} {bandwidthDownBytes} {bandwidthUpBytes}",
        "on-click-right": "cmst",
        "tooltip-format-wifi": "WiFi ({ifname})\nSSID: {essid}\nStrength: {signalStrength}%",
        "tooltip-format-ethernet": "Ethernet ({ifname})",
        "format-disconnected": "{icon}",
        "format-disabled": "Network disabled {icon}",
        "tooltip-format-disconnected": "Not connected",
        "format-alt": "{icon} IP Address: {ipaddr}"
    },
    "pulseaudio#output": {
     	"format-muted": "<span font=\"Symbols Nerd Font Mono\" rise='-15000'>󰖁</span>",
        "format": "<span font=\"Symbols Nerd Font Mono\" rise='1400'>{icon}</span> {volume}%",
        "format-icons": {
            "headphone": "",
            "bluetooth": "󰥰",
            "headset": "󰋎",
            "phone": "󰏲",
            "portable": "󰏲",
            "car": "󰄋",
            "hdmi": "󰡁",
            "default": ["󰕿", "󰖀", "󰕾"]
        },
        "on-click-right": "pavucontrol -t 3",
        "on-click": "amixer -q set Master toggle",
        "ignored-sinks": ["Easy Effects Sink", "CavaLoopback", "CavaSink"]
      },
    "pulseaudio#input": {
    	"format-source": "<span font=\"Symbols Nerd Font Mono\" rise='1400'>󰍬</span> {volume}%",
    	"format-source-muted": "<span font=\"Symbols Nerd Font Mono\" rise='-15000'>󰍭</span>",
        "format": "{format_source}",
        "on-click-right": "pavucontrol -t 4",
        "on-click": "amixer -q set Capture toggle",
        "on-scroll-up": "amixer set Capture 1%+",
        "on-scroll-down": "amixer set Capture 1%-",
        "ignored-sinks": ["Easy Effects Sink", "CavaLoopback", "CavaSink"]
      },            
    "wlr/taskbar": {
        "format": "{icon}",
        "icon-size": 28,
        "tooltip-format": "{title}",
        "on-click": "minimize-raise",
        "on-click-right": "close",
        "app_ids-mapping": {
        	"steamwebhelper": "steam"
        },
        "sort-by-app-id": true
    },
	"custom/swaync": {
	"tooltip": true,
	"format": "{icon} {0}",
	"format-icons": {
		"notification": "<span foreground='red'><sup></sup></span>",
		"none": "",
		"dnd-notification": "<span foreground='red'><sup></sup></span>",
		"dnd-none": "",
		"inhibited-notification": "<span foreground='red'><sup></sup></span>",
		"inhibited-none": "",
		"dnd-inhibited-notification": "<span foreground='red'><sup></sup></span>",
		"dnd-inhibited-none": ""
	},
	"return-type": "json",
	"exec-if": "which swaync-client",
	"exec": "swaync-client -swb",
	"on-click": "swaync-client -t",
	"on-click-right": "swaync-client -d",
	"escape": true,
	}	
}
